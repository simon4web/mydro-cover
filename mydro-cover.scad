//**************Constants********************
/* [Global] */

/* [Basic Settings] */

// The thickness of the Boxs' walls [mm].
WALL_THICKNESS = 1.8;

// The size of the cable hole [mm].
CABLE_HOLE_DIAMETER = 5;

// Radius for smoother [mm].
SMOOTHER_RADIUS = 1.5;


//****************Variables (in mm)******************
sen_width = 22.55;
sen_length_to_cover = 25;
sen_length_top_to_nose = 18;
sen_nose_outer_length = 3.75;
sen_nose_inner_length = 2.4;
sen_nose_hight = 1.4;
sen_thickness = 1.2;
sen_abs_thickness = sen_thickness+CABLE_HOLE_DIAMETER-SMOOTHER_RADIUS;

box_abs_length = sen_width+2*WALL_THICKNESS;
box_abs_width = sen_abs_thickness+2*WALL_THICKNESS+2*SMOOTHER_RADIUS;
box_abs_height = WALL_THICKNESS+sen_length_to_cover;

box_hole_length = sen_width;
box_hole_width = sen_abs_thickness+2*SMOOTHER_RADIUS;

holder_width = 2;
holder_length = 1;

cable_bend_radius = 1.5* CABLE_HOLE_DIAMETER;

cable_mount_height = (cable_bend_radius+WALL_THICKNESS+holder_length+CABLE_HOLE_DIAMETER/2);
cable_mount_width = cable_bend_radius+box_abs_width/2+ 2*SMOOTHER_RADIUS;

cable_hole_y = SMOOTHER_RADIUS+WALL_THICKNESS+sen_thickness+CABLE_HOLE_DIAMETER/2;
cable_hole_z = WALL_THICKNESS+holder_length+CABLE_HOLE_DIAMETER/2;

a_tan = cable_mount_height - cable_hole_z;
g_tan = cable_hole_y - (box_abs_width/2);

solder_space_size = 7.5;

//****************Geometry*******************

$fa=0.15; $fs=0.15;
Box();
BoxText();
//****************Modules********************

//Main Modules
module Box()
{
  difference()
  {
    union()
    {
      BaseBox();
      CableMountBody();
    }
    CableCutOut();
  }
  //Top();
}

module Top()
{
  intersection()
  {
    BaseBoxBody();
    BaseBoxTop();
  }
}

module BaseBox()
{
  union()
  {
    difference()
    {
      BaseBoxBody();
      BaseBoxHole();
      BaseBoxTop();
      BaseBoxSolderSpace();
    }
    BaseBoxNoseRight();
    BaseBoxNoseLeft();
  }
}

module BaseBoxBody()
{
  union()   //base
  {
    minkowski()
    {
      cube([box_abs_length-2*SMOOTHER_RADIUS, box_abs_width-2*SMOOTHER_RADIUS,box_abs_height-2*SMOOTHER_RADIUS]);
      cylinder(h = SMOOTHER_RADIUS,r = SMOOTHER_RADIUS);
    }
    minkowski()
    {
      cube([box_abs_length-2*SMOOTHER_RADIUS, box_abs_width-2*SMOOTHER_RADIUS,box_abs_height-2*SMOOTHER_RADIUS]);
      translate([0,0,SMOOTHER_RADIUS])
      {
        sphere(r = SMOOTHER_RADIUS);
      }
    }
  }
}

module BaseBoxHole()
{
  translate([WALL_THICKNESS,WALL_THICKNESS,WALL_THICKNESS]) //hole
  {
    difference() {
      minkowski()
      {
          cube([box_hole_length-2*SMOOTHER_RADIUS, box_hole_width-2*SMOOTHER_RADIUS,box_abs_height/2]);
          cylinder(h = box_abs_height/2,r = SMOOTHER_RADIUS);
      }
      translate([-SMOOTHER_RADIUS,-SMOOTHER_RADIUS,0])
      {
        cube([box_hole_length, SMOOTHER_RADIUS,box_abs_height]);
      }
      translate([-SMOOTHER_RADIUS,sen_thickness,0])
      {
        cube([holder_width, box_abs_width*2,holder_length]);
      }
      translate([-SMOOTHER_RADIUS+sen_width-holder_width,sen_thickness,0])
      {
        cube([holder_width, box_abs_width*2,holder_length]);
      }
    }
  }
}

module BaseBoxTop()
{
  translate([WALL_THICKNESS/2,WALL_THICKNESS/2,sen_length_to_cover]) //top mount
  {
    difference()
    {
      minkowski()
      {
          cube([box_hole_length+WALL_THICKNESS-2*SMOOTHER_RADIUS, box_hole_width+WALL_THICKNESS-2*SMOOTHER_RADIUS,box_abs_height-SMOOTHER_RADIUS]);
          cylinder(h = 2*SMOOTHER_RADIUS,r = SMOOTHER_RADIUS);
      }
      translate([-SMOOTHER_RADIUS-WALL_THICKNESS,-SMOOTHER_RADIUS,-1])
      {
        cube([box_hole_length+WALL_THICKNESS*5, SMOOTHER_RADIUS+WALL_THICKNESS/2,box_abs_height]);
      }
      translate([-SMOOTHER_RADIUS,WALL_THICKNESS/2,0])
      {
        cube([WALL_THICKNESS/2, sen_thickness,WALL_THICKNESS]);
      }
      translate([-SMOOTHER_RADIUS+sen_width+WALL_THICKNESS/2,WALL_THICKNESS/2,0])
      {
        cube([WALL_THICKNESS/2, sen_thickness,WALL_THICKNESS]);
      }
    }
  }
}

module BaseBoxNoseLeft()
{
  translate([WALL_THICKNESS-SMOOTHER_RADIUS,WALL_THICKNESS,WALL_THICKNESS+sen_length_top_to_nose])rotate ([0,-90,-90])linear_extrude(height=sen_thickness)
  {
    polygon(points=[[0,0],[sen_nose_outer_length,0],[sen_nose_outer_length/2+sen_nose_inner_length/2,sen_nose_hight],[sen_nose_outer_length/2-sen_nose_inner_length/2,sen_nose_hight]]);
  }
}

module BaseBoxNoseRight()
{
  translate([WALL_THICKNESS-SMOOTHER_RADIUS+box_hole_length,WALL_THICKNESS,WALL_THICKNESS+sen_length_top_to_nose])rotate ([0,-90,-90]) linear_extrude(height=sen_thickness)
  {
    polygon(points=[[0,0],[sen_nose_outer_length,0],[sen_nose_outer_length/2+sen_nose_inner_length/2,-sen_nose_hight],[sen_nose_outer_length/2-sen_nose_inner_length/2,-sen_nose_hight]]);
  }
}

module BaseBoxSolderSpace()
{
  translate([box_abs_length/2-SMOOTHER_RADIUS,WALL_THICKNESS+0.0001,solder_space_size*0.8+WALL_THICKNESS]) rotate ([90,0,0]) linear_extrude(height=SMOOTHER_RADIUS)
  {
    circle(r = solder_space_size/2, $fn = 100);
    translate([-solder_space_size/2, -solder_space_size*0.8, 0]) square([solder_space_size,solder_space_size*0.8]);
  }
}


module CableMountBody()
{
  difference()
  {
    translate([box_abs_length-2*SMOOTHER_RADIUS,0,0]) union()   //base
    {
      minkowski()
      {
        cube([cable_mount_width-2*SMOOTHER_RADIUS, box_abs_width-2*SMOOTHER_RADIUS,cable_mount_height-2*SMOOTHER_RADIUS]);
        cylinder(h = SMOOTHER_RADIUS,r = SMOOTHER_RADIUS);
      }
      minkowski()
      {
        cube([cable_mount_width-2*SMOOTHER_RADIUS, box_abs_width-2*SMOOTHER_RADIUS,cable_mount_height-2*SMOOTHER_RADIUS]);
        translate([0,0,SMOOTHER_RADIUS])
        {
          sphere(r = SMOOTHER_RADIUS);
        }
      }
    }
    BaseBoxBody();
  }
}

module CableCutOut()
{
  l_hori=WALL_THICKNESS+SMOOTHER_RADIUS;
  l_vert=CABLE_HOLE_DIAMETER/2;
  translate([-SMOOTHER_RADIUS+WALL_THICKNESS+box_hole_length-SMOOTHER_RADIUS,WALL_THICKNESS+sen_thickness+CABLE_HOLE_DIAMETER/2,CABLE_HOLE_DIAMETER/2+WALL_THICKNESS+holder_length])
  {
    rotate([atan(g_tan/a_tan),0,0])
    {
      rotate([0,90,0])cylinder(r=CABLE_HOLE_DIAMETER/2,h=l_hori);
      translate([l_hori,0,0]) CableCutOut90Knee();
      translate([cable_bend_radius+l_hori,0,cable_bend_radius]) cylinder(r=CABLE_HOLE_DIAMETER/2,h=l_vert);
    }
  }
}

module CableCutOut90Knee()
{
  translate([0,0,cable_bend_radius])difference()
  {
    rotate ([90,0,0]) rotate_extrude(convexity = 5) translate([cable_bend_radius, 0, 0]) circle(r = CABLE_HOLE_DIAMETER/2, $fn = 50);
    translate([-1.5*cable_bend_radius,-1.5*cable_bend_radius,0]) cube([3*cable_bend_radius,3*cable_bend_radius,3*cable_bend_radius]);
    translate([-3*cable_bend_radius,-1.5*cable_bend_radius,-1.5*cable_bend_radius]) cube([3*cable_bend_radius,3*cable_bend_radius,3*cable_bend_radius]);
  }
}

module BoxText()
{
  translate([34.5,-SMOOTHER_RADIUS,2+cable_mount_height*0.50])rotate ([90,180,0]) linear_extrude(height=0.3) text(text="MYDRO",font="TeX Gyre Adventor:style=Regular",size=cable_mount_height*0.50);
}
